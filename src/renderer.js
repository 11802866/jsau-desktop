/* global window */
/* global document */
/* global location */

'use strict'

const {ipcRenderer} = window.electron

const messageList = document.getElementById('message-list')
const messageForm = document.getElementById('message-form')
const senderInput = document.getElementById('sender')
const contentInput = document.getElementById('content')
const sendButton = document.getElementById('send-button')

let messages = []

const emojiMapping = {
    ':grinning:': '😀',
    ':blush:': '😊',
    ':thumbsup:': '👍',
    ':heart:': '❤️',
    ':fire:': '🔥',
    ':rocket:': '🚀',
    ':eyes:': '👀',

}

const updateMessageList = (newMessages) => {
    messageList.innerHTML = ''

    if (Array.isArray(newMessages)) {
        newMessages.forEach((message, index) => {
            const listItem = document.createElement('li')
            listItem.textContent = `${message.sender}: ${message.content}`

            const deleteButton = document.createElement('button')
            deleteButton.textContent = 'Supprimer'
            deleteButton.classList.add('delete-button')

            deleteButton.addEventListener('click', () => deleteMessage(index))
            listItem.appendChild(deleteButton)

            messageList.appendChild(listItem)
        })
    }
}

const deleteMessage = async(index) => {
    try {
        const updatedMessages = await ipcRenderer.invoke('delete-message', index)
        messages = updatedMessages
        updateMessageList(messages)
    } catch (error) {
        console.error('Erreur lors de la suppression du message côté rendu :', error)
    }
}

const sendMessage = async() => {
    const sender = senderInput.value
    let content = contentInput.value

    Object.entries(emojiMapping).forEach(([emojiCode, emojiChar]) => {
        content = content.replace(new RegExp(emojiCode, 'g'), emojiChar)
    })

    if (sender && content) {
        try {

            const updatedMessages = await ipcRenderer.invoke('send-message', {sender, content})
            messages = updatedMessages
            updateMessageList()
            return Promise.resolve()
        } catch (error) {
            console.error('Erreur lors de send-message côté rendu :', error)
            return Promise.reject(error)
        }
    }

    return Promise.resolve()
}

sendButton.addEventListener('click', () => {
    sendMessage()
    location.reload()
})

window.addEventListener('load', async() => {
    try {
        const message = await window.electron.readMessages()

        if (Array.isArray(message) && message.length > 0) {
            updateMessageList(message)
        }
    } catch (error) {
        console.error('Erreur lors du chargement des messages au démarrage :', error)
    }
})

ipcRenderer.on('update-messages', (event, updatedMessages) => {
    messages = updatedMessages
    updateMessageList(messages)
})

messageForm.addEventListener('submit', (event) => {
    event.preventDefault()
    sendMessage()

    location.reload()
})

module.exports = {
    ipcRenderer,
    updateMessageList,
    deleteMessage,
    sendMessage,
}

