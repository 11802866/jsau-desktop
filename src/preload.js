'use strict'

const {contextBridge, ipcRenderer} = require('electron')

contextBridge.exposeInMainWorld('electron', {
    ipcRenderer,
    readMessages: () => {
        return ipcRenderer.invoke('read-messages')
    }
})

