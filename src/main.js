'use strict'

const {app, BrowserWindow, ipcMain} = require('electron')
const path = require('path')
const fs = require('fs').promises

const messagesFile = path.join(__dirname, 'messages.json')
let mainWindow

const readMessages = async() => {
    try {
        const data = await fs.readFile(messagesFile, 'utf8')
        const parsedData = JSON.parse(data)
        return Array.isArray(parsedData.messages) ? parsedData.messages : []
    } catch (err) {
        console.error('Erreur lors de la lecture des messages :', err)
        return []
    }
}

const writeMessages = async(messages) => {
    try {
        await fs.writeFile(messagesFile, JSON.stringify({messages}, null, 2), 'utf8')
    } catch (err) {
        console.error('Erreur lors de l’écriture des messages :', err)
        throw err
    }
}

const createWindow = () => {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            hardwareAcceleration: false
        }
    })

    mainWindow.loadFile('./src/index.html')
}

app.whenReady().then(() => {
    return new Promise(async(resolve, reject) => {
        try {
            await fs.access(messagesFile)
            createWindow()
            resolve()
        } catch (err) {
            console.error('Le fichier messages.json n\'existe pas, il sera créé.')
            await fs.writeFile(messagesFile, JSON.stringify({messages: []}, null, 2), 'utf8')
            createWindow()
            resolve()
        }
    })
})

ipcMain.handle('send-message', async(event, message) => {
    const messages = await readMessages()
    messages.push(message)
    await writeMessages(messages)

    mainWindow.webContents.send('update-messages', messages)

    return messages
})

ipcMain.handle('read-messages', async() => {
    return readMessages()
})

ipcMain.handle('delete-message', async(event, index) => {
    try {
        const messages = await readMessages()
        if (index >= 0 && index < messages.length) {
            messages.splice(index, 1)
            await writeMessages(messages)

            mainWindow.webContents.send('update-messages', messages)
        }
        return messages
    } catch (error) {
        console.error('Erreur lors de la suppression du message côté principal :', error)
        throw error
    }
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})
